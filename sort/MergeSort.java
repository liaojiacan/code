class MergeSort{

	public void sort(int[] array ,int size){
		int [] workspace =new int[size];
		recMergeSort(array,workspace,0,size-1);
	}
	public void display(int[] workspace ,int size){
		for (int i=0;i<size ;i++ ) {
			System.out.print(workspace[i]+" ");
		}
		System.out.println();
	}
	public void recMergeSort(int []array,int [] workspace,int lowerBound,int upperBound){

		if (upperBound==lowerBound) return;
		else{
			int mid = (lowerBound+upperBound)/2;
			recMergeSort(array,workspace,lowerBound,mid);
			recMergeSort(array,workspace,mid+1,upperBound);
			merge(array,workspace,lowerBound,mid+1,upperBound);
		}
	}

	public void merge(int[] array,int[] workspace,int lowerPtr,int highPtr,int upperBound){
		int j = 0;
		int lowerBound=lowerPtr;
		int mid = highPtr-1;
		int n = upperBound-lowerBound+1;

		//进行合并

		while(lowerPtr<=mid&&highPtr<=upperBound){
			if (array[lowerPtr]<array[highPtr]) {
				workspace[j++]=array[lowerPtr++];
			}else{
				workspace[j++]=array[highPtr++];
			}
		}

		//合并剩下的。

		while(lowerPtr<=mid){
			workspace[j++]=array[lowerPtr++];
		}
		while(highPtr<=upperBound){
			workspace[j++]=array[highPtr++];
		}

		for(j=0;j<n;j++){
			array[lowerBound+j]=workspace[j];
		}
	}
}