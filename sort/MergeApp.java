public class MergeApp{

	public static void merge(int[] arrayA ,int sizeA,int[] arrayB,int sizeB,int[] arrayC){
	
		int index_a=0,index_b=0,index_c=0;
		//合并相同长度的部分
		while(index_a<sizeA&&index_b<sizeB){
			if (arrayA[index_a]<arrayB[index_b]) {
				arrayC[index_c++]=arrayA[index_a++];
			}else{
				arrayC[index_c++]=arrayB[index_b++];
			}
		}
		//如果arrayA比较长
		while(index_a<sizeA){
			arrayC[index_c++]=arrayA[index_a++];
		}
		//如果arrayB比较长
		while(index_b<sizeB){
			arrayC[index_c++]=arrayB[index_b++];
		}

	}

	public static void display(int[] array,int size){
		for (int i=0;i<size ;i++ ) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}
	public static void main(String[] args) {
		int [] arrayA={23,47,81,95};
		int [] arrayB={7,14,39,55,62,74};
		int [] arrayC=new int[10];

		merge(arrayA,4,arrayB,6,arrayC);
		display(arrayC,10);
	}	
}

