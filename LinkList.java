class LinkList{
	private Link first;

	public void insert(int item){

		Link temp=new Link();
		temp.data = item;
		//判断链表是不是空的。
		if (isEmpty()) {
			first=temp;
			return ;
		}
		//找到最后的节点，插入到最后
		Link current =first;
		while(current.next!=null){
			current=current.next;
		}

		current.next=temp;
		temp.next=null;
	}
	//找到关键字所在的位置
	public int find(int item){
		Link current=first;
		int cur=0;
		while(current.next!=null){
			cur++;
			if (current.data==item) {
				return cur;
			}

			current=current.next;
		}

		if (current.data==item) {
			return ++cur;
		}

		return -1;
	}
	//更新指定下标的数值
	public boolean update(int tagert , int item){
		Link current=first;
		try{
		for (int i=0;i<tagert-1;i++) {
			current=current.next;
		}
		current.data= item;
		}catch(Exception e){System.out.print("超出链表下标"); return false;}

		return true;
	}


	public void dispaly(){

		Link current=first;
		while (current.next!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
		System.out.print(current.data+"\n");
		
	}
	public boolean isEmpty(){
		return first==null;
	}
}