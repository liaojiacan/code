class FindKMax{
	public static void main(String[] args) {
		FindKMax finder = new FindKMax();
		int a[] = {1,2,666,7833,999,45,11,678,33,445};
		int kvalue= finder.find(a,0,a.length-1,1);
		System.out.println(kvalue);
	}

	/*求枢纽**/
	public int sort(int a[] , int low ,int high){
		int povit = a[low];
		if (low < high) {

			while(a[high]>=povit&&low<high)
				high--;

			a[low] = a[high];
			while(a[low]<=povit&&low<high)
				low++;
			a[high] = a[low];
		}	

		a[low] = povit; //把枢纽的值交换到低位
		return low;
	}

	public int find(int a[],int low,int high,int k){
		if(low>=high)
			return a[low];
		else {
			
		int mid = sort(a,low,high);
		if (mid>k) {
			return find(a,low,mid-1,k);
		}else if(mid<k){
			return find(a,mid+1,high,k);
		}else{
			return a[mid];
		}
		}
	}


}